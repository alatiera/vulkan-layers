#!/usr/bin/env bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2018-2022 Collabora, Ltd. and the Monado contributors
#
# Requires some environment variables (normally set by CI)

CI_COMMIT_SHA=${CI_COMMIT_SHA:-HEAD}
echo "Will make source tarball for ${CI_COMMIT_SHA}"
CI_DIR=$(cd "$(dirname "$0")" && pwd)

set -euo pipefail
datestamp=$(date --utc "+%Y%m%d")
FULL_VER=$("${CI_DIR}/../get-version.sh")

if [ -z "$FULL_VER" ];then
    echo "Could not figure out version!"
    exit 1
fi

echo "Got full version $FULL_VER"

# Generate source tarball
git-archive-all -v --prefix "monado-vulkan-layers" "../monado-vulkan-layers_$FULL_VER.orig.tar.xz"

mv "../monado-vulkan-layers_$FULL_VER.orig.tar.xz" .

# Stash these for later use
echo "FULL_VER=$FULL_VER" > sourcetarball.env
echo "DATESTAMP=$datestamp" >> sourcetarball.env
