// Copyright 2019-2022, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Environmental helpers.
 * @author Jakob Bornecrantz <jakob@collabora.com>
 *
 * Copied from Monado.
 * Debug get option helpers heavily inspired from mesa ones.
 */

#include "env_option.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


static const char *
os_getenv(const char *name)
{
	return getenv(name);
}

const char *
debug_get_option(const char *name, const char *_default)
{
	const char *raw = getenv(name);
	const char *ret;

	if (raw == NULL) {
		ret = _default;
	} else {
		ret = raw;
	}

	return ret;
}

bool
debug_string_to_bool(const char *raw)
{
	bool ret;
	if (raw == NULL) {
		ret = false;
	} else if (!strcmp(raw, "false")) {
		ret = false;
	} else if (!strcmp(raw, "FALSE")) {
		ret = false;
	} else if (!strcmp(raw, "off")) {
		ret = false;
	} else if (!strcmp(raw, "OFF")) {
		ret = false;
	} else if (!strcmp(raw, "no")) {
		ret = false;
	} else if (!strcmp(raw, "NO")) {
		ret = false;
	} else if (!strcmp(raw, "n")) {
		ret = false;
	} else if (!strcmp(raw, "N")) {
		ret = false;
	} else if (!strcmp(raw, "f")) {
		ret = false;
	} else if (!strcmp(raw, "F")) {
		ret = false;
	} else if (!strcmp(raw, "0")) {
		ret = false;
	} else {
		ret = true;
	}
	return ret;
}

bool
debug_get_bool_option(const char *name, bool _default)
{
	const char *raw = os_getenv(name);
	bool ret = raw == NULL ? _default : debug_string_to_bool(raw);

	return ret;
}
